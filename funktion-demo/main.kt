import io.vertx.core.json.JsonObject
import garden.bots.allowed.*

{params: JsonObject ->

  println("this is a demo")

  val txt = Plugin("star.trek.Spocky").call("hi") as String
  val message = Plugin("wonderful.things.DarthVader").call("getQuote") as String
  println(message)

  //JsonObject().put("message", "👋 yo, ${Babs().hi()}").put("spocky", message)
  JsonObject()
    .put("message", "👋 yo, ${Babs().hi()}")
    .put("spocky", txt)
    .put("vader", message)
    .put("k33g","hello world")

}
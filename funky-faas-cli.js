#!/usr/bin/env node

/*
  FUNKY FAAS CLI

*/

const axios = require('axios')
const argv = require('yargs').argv
const monet = require('monet')
const fs = require('fs')
const path = require('path')
const Minio = require('minio')
const chalk = require('chalk');

/*
const minioClient = new Minio.Client({
    endPoint: 'play.minio.io',
    port: 9000,
    useSSL: true,
    accessKey: 'Q3AM3UQ867SPQQA43P2F',
    secretKey: 'zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG'
});
*/

const Maybe = monet.Maybe
const Validation = monet.Validation

/*
  
  CLI Environment variables:
  -------------------------------------------------

  export FUNKTIONS_ADMIN_TOKEN="ilovepanda"
  export ACCESS_KEY="battlestar"
  export SECRET_KEY="galactica"
  export FUNKTIONS_BUCKET="funky-sandbox"
  export OBJECT_STORAGE_DOMAIN="localhost"
  export OBJECT_STORAGE_PORT=9000
  export FUNKY_FAAS_URL="http://localhost:8080"

  CLI Parameters:
  -------------------------------------------------
  # store from directory
  node funky-faas-cli.js \
  --fkdir=funtion-demo \
  --action=store

  ./funky-faas-cli.sh --action=store --fkdir=funktion-webapp

  # deploy from object storage to the platform
  node funky-faas-cli.js \
  --fknamever=hello-world:0.0.3 \
  --action=deploy

  ./funky-faas-cli.sh --action=deploy --fknamever=web-app:0.0.0

  # remove from the platform
  node funky-faas-cli.js \
  --fknamever=hello-world:0.0.3 \
  --action=remove

  ./funky-faas-cli.sh --action=remove --fknamever=web-app:0.0.0

  # command samples:
  ./funky-faas-cli.sh --action=remove --fknamever=hello-world:zed
  ./funky-faas-cli.sh --action=deploy --fknamever=hello-world:zed
  ./funky-faas-cli.sh --action=store --fkdir=funktion-demo-3
*/

const funktionsAdminToken = Maybe.fromNull(process.env["FUNKTIONS_ADMIN_TOKEN"])
const objectStorageAccessKey = Maybe.fromNull(process.env["ACCESS_KEY"])
const objectStorageSecretKey = Maybe.fromNull(process.env["SECRET_KEY"])
const objectStorageDomain = Maybe.fromNull(process.env["OBJECT_STORAGE_DOMAIN"])
const objectStoragePort = Maybe.fromNull(process.env["OBJECT_STORAGE_PORT"])
const funkyFaaSUrl = Maybe.fromNull(process.env["FUNKY_FAAS_URL"])
const funktionsBucket = Maybe.fromNull(process.env["FUNKTIONS_BUCKET"])

const funktionDirectory = Maybe.fromNull(argv.fkdir)
const funktionNameVersion = Maybe.fromNull(argv.fknamever)
const action = Maybe.fromNull(argv.action)

let errorMessages = []



let checkList = (errorMessages)  => {
  return errorMessages.length > 0
    ? Validation.Fail("😡 errors: \n" + errorMessages.join("\n"))
    : Validation.Success("🙂")
}

let funktionInformation = (directory) => {
    try {
      return Validation.Success(
        JSON.parse(
          fs.readFileSync(`${directory}/deploy.json`, 'utf8')
        )
      )
    } catch(e) {
      Validation.Fail(`😡 error: ${e.message}`)
    }
}

let getMinioClient = () => {
  try {
    const minioClient = new Minio.Client({
      endPoint: objectStorageDomain.some(),
      port: parseInt(objectStoragePort.some()),
      useSSL: false,
      accessKey: objectStorageAccessKey.some(),
      secretKey: objectStorageSecretKey.some()
    });    
    return minioClient
  } catch(err) {
    console.log(chalk.red(err))
    process.exit(1)
  }
}

//TODO: sync the task or chain the callbacks
let storeFunktion = (minioClient) => {
  funktionDirectory.isNone() ? errorMessages.push("> funktion directory cannot be null") : "🎉"
  checkList(errorMessages).cata(
    err => {
      //console.log(chalk.red(err))
      console.log(chalk.red(`😡 > store stage: ${err}`))
      process.exit(1)
    }, 
    value => {
      console.log(chalk.yellow(`> store ${funktionDirectory.some()}`))
      // get funktion data from deploy.json
      funktionInformation(funktionDirectory.some()).cata(
        err => {
          console.log(chalk.red(err))
          process.exit(1)
        }, 
        value => {
          console.log(chalk.yellow(`  > ${value.name} ${value.version} ${value.type}`))
          console.log(chalk.yellow(`  > ${value.description}`))

          minioClient.bucketExists(funktionsBucket.some(), function(err, exists) {
            if(err) {
              console.log(chalk.red(err))
              process.exit(1)
            } else {
              if(exists==false) {
                console.log(chalk.red(`👋 > ${funktionsBucket.some()} Bucket does not exist.`))
                minioClient.makeBucket(funktionsBucket.some())
                console.log(chalk.green(`👋 > ${funktionsBucket.some()} Bucket created.`))
              } else {
                console.log(chalk.green(`👋 > ${funktionsBucket.some()} Bucket already exists.`))
              }
              // here the rest of the commands
              // copy the funktion
              console.log(chalk.blue(`👋 > copying ${value.name}:${value.version} to ${funktionsBucket.some()} ...`))

              // copy deploy.json
              const deployFile = `${funktionDirectory.some()}/deploy.json`
              const deployFileMetaData = {} //TODO

              // ⚠️ asynchronous task
              minioClient.fPutObject(funktionsBucket.some(), `${value.name}:${value.version}:deploy.json`, deployFile, deployFileMetaData, function(err, etag) {
                if(err) {
                  console.log(chalk.red(`😡 > when copying deploy.json: ${err}`))
                  process.exit(1)
                } else {
                  return console.log(chalk.green(`deploy.json ${etag}`))
                }
              })

              // copy main.kt
              const mainFile = `${funktionDirectory.some()}/main.kt`
              const mainFileMetaData = {} //TODO

              // ⚠️ asynchronous task
              minioClient.fPutObject(funktionsBucket.some(), `${value.name}:${value.version}:main.kt`, mainFile, mainFileMetaData, function(err, etag) {
                if(err) {
                  console.log(chalk.red(`😡 > when copying main.kt: ${err}`))
                  process.exit(1)
                } else {
                  return console.log(chalk.green(`main.kt ${etag}`))
                }
              })     

              // Load plugins from disk and then copy them to the store
              let plugins = fs.readdirSync(`${funktionDirectory.some()}/plugins`).filter(file => path.extname(file) == ".jar")
              console.log(chalk.yellow(`plugins: ${plugins}`))

              plugins.forEach(plugin => {
                const pluginFile = `${funktionDirectory.some()}/plugins/${plugin}`
                const pluginFileMetaData = {} //TODO
                // ⚠️ asynchronous task
                minioClient.fPutObject(funktionsBucket.some(), `${value.name}:${value.version}:plugins:${plugin}`, pluginFile, pluginFileMetaData, function(err, etag) {
                  if(err) {
                    console.log(chalk.red(`😡 > when copying ${plugin}: ${err}`))
                    process.exit(1)
                  } else {
                    return console.log(chalk.green(`${plugin} ${etag}`))
                  }
                })                     
              })

              // Load modules from disk and then copy them to the store
              let modules = fs.readdirSync(`${funktionDirectory.some()}/modules`).filter(file => path.extname(file) == ".kt")
              console.log(chalk.yellow(`modules: ${modules}`))

              modules.forEach(module => {
                const moduleFile = `${funktionDirectory.some()}/modules/${module}`
                const moduleFileMetaData = {} //TODO
                // ⚠️ asynchronous task
                minioClient.fPutObject(funktionsBucket.some(), `${value.name}:${value.version}:modules:${module}`, moduleFile, moduleFileMetaData, function(err, etag) {
                  if(err) {
                    console.log(chalk.red(`😡 > when copying ${module}: ${err}`))
                    process.exit(1)
                  } else {
                    return console.log(chalk.green(`${module} ${etag}`))
                  }
                })                     
              })
            } // end of else (does bucket exist?)
          }) // end of callback + minioClient.bucketExists()
        } // end of value => of cata
      ) // end of cata
    }
  ) // end of checklist cata
} // end of storeFunktion


let deployFunktion = (minioClient) => {
  funktionNameVersion.isNone() ? errorMessages.push("> funktion name:version cannot be null") : "🎉"
  checkList(errorMessages).cata(
    err => {
      //console.log(chalk.red(err))
      console.log(chalk.red(`😡 > deploy stage: ${err}`))
      process.exit(1)
    }, 
    value => {
      console.log(chalk.green(`> deploying ${funktionNameVersion.some()}`))
      let funktionName = funktionNameVersion.some().split(":")[0]
      let funktionVersion = funktionNameVersion.some().split(":")[1]
      axios({
        method: `GET`,
        url:  `${funkyFaaSUrl.some()}/deploy/${funktionName}/${funktionVersion}`,
        headers: {
          "Content-Type": "application/json",
          "FUNKTIONS_ADMIN_TOKEN": funktionsAdminToken.some()
        }
      })
      .then(res => res.data)
      .then(res => { // success
        console.log(chalk.yellow(`🤔 > deploy stage:`))
        if(res.status=='failure') {
          console.log(chalk.red(`😡 > when deploying ${res.funktionStorageId}: ${res.error}`))
        } else {
          //TODO: use sse or something else to follow the deployment
          console.log(chalk.green(`😃 > deployment of ${res.funktionStorageId} as started`))
          console.log(res)
        }
      })
      .catch(err => { // failure
        console.log(chalk.red(`😡 > deploy stage: ${err}`))
      })
    }
  )  
}


let removeFunktion = (minioClient) => {
  funktionNameVersion.isNone() ? errorMessages.push("> funktion name:version cannot be null") : "🎉"
  checkList(errorMessages).cata(
    err => {
      //console.log(chalk.red(err))
      console.log(chalk.red(`😡 > remove stage: ${err}`))
      process.exit(1)
    }, 
    value => {
      console.log(chalk.blue(`> removing ${funktionNameVersion.some()}`))

      let funktionName = funktionNameVersion.some().split(":")[0]
      let funktionVersion = funktionNameVersion.some().split(":")[1]

      ///processes/kill/all/:name/:version

      axios({
        method: `DELETE`,
        url:  `${funkyFaaSUrl.some()}/processes/kill/all/${funktionName}/${funktionVersion}`,
        headers: {
          "Content-Type": "application/json",
          "FUNKTIONS_ADMIN_TOKEN": funktionsAdminToken.some()
        }
      })
      .then(res => res.data)
      .then(res => { // success
        console.log(chalk.yellow(`🤔 > removal stage:`))
        if(res.status==='failure') {
          console.log(chalk.red(`😡 > when removing ${funktionName}:${funktionVersion}: ${res.error}`))
        } else {
          //TODO: use sse or something else to follow the deployment
          console.log(chalk.green(`😃 > removal of ${funktionName}:${funktionVersion} as started`))
          console.log("result:",res)

          // removing assets funktion from the object storage
          let stream = minioClient.listObjects(funktionsBucket.some(),`${funktionName}:${funktionVersion}`, true)

          stream.on('data', objects => {
            for(let obj in objects) {
              if(obj==="name") {
                minioClient.removeObject(funktionsBucket.some(), objects["name"], err => {
                  if (err) {
                    return console.log(chalk.red(`😡 > when removing ${objects["name"]}: ${err}`))
                  }
                  console.log(chalk.green(`😃 > removal of ${objects["name"]} from storage ✅`))
                })
              }
            }

          })
          stream.on(
            'error',
            err => {
              console.log(chalk.red(`😡 > when removing ${funktionName}:${funktionVersion}: ${err}`))
            }
          )


        }
      })
      .catch(err => { // failure
        console.log(chalk.red(`😡 > removal stage: ${err}`))
      })

    }
  )
}



//console.log(chalk.blue('Hello world!'));

action.isNone() ? errorMessages.push("> action cannot be null") : "🎉"
funktionsAdminToken.isNone() ? errorMessages.push("> admin token cannot be null") : "🎉"
objectStorageAccessKey.isNone() ? errorMessages.push("> object storage access key cannot be null") : "🎉"
objectStorageSecretKey.isNone() ? errorMessages.push("> object storage secret key cannot be null") : "🎉"
objectStorageDomain.isNone() ? errorMessages.push("> object storage domain cannot be null") : "🎉"
objectStoragePort.isNone() ? errorMessages.push("> object storage port cannot be null") : "🎉"

funkyFaaSUrl.isNone() ? errorMessages.push("> funky-paas-platform url cannot be null") : "🎉"
funktionsBucket.isNone() ? errorMessages.push("> object storage bucket name cannot be null") : "🎉"


checkList(errorMessages).cata(
  err => {
    console.log(chalk.red(err))
    process.exit(1)
  }, 
  value => {
    
    console.log(chalk.green("> checkList ok"))
    let minioClient = getMinioClient()


    switch(action.some()) {
      case "store":
        storeFunktion(minioClient)
        break;
      case "deploy":
        deployFunktion(minioClient)
        break;
      case "remove":
        removeFunktion(minioClient)
        break;
      case "generate":
        console.log("> not implemented")
        break;
      default:
        console.log("> command unknown")
    }
  }  
)




